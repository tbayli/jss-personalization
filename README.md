# Sitecore JSS Personalization POC

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=tmamedbekov_jss-personalization&metric=alert_status)](https://sonarcloud.io/dashboard?id=tmamedbekov_jss-personalization)

```
jss start
npm run build
jss deploy app
```

Then set up some campaigns

And personalize your components

![alt text](https://raw.githubusercontent.com/tmamedbekov/jss-personalization/master/data/media/img/orlando/personalization.png)
